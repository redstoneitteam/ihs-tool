﻿namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    partial class EnerdeqMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnerdeqMain));
            this.cmbExtractType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbState = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCounty = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStateCode = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCountyCode = new System.Windows.Forms.ComboBox();
            this.chkProductionstatus = new System.Windows.Forms.CheckBox();
            this.dtpCompletionDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbHoleDirection = new System.Windows.Forms.ComboBox();
            this.btnGetDatatoDB = new System.Windows.Forms.Button();
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDataUsername = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDataServer = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDataPwd = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDataDB = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numRecordSize = new System.Windows.Forms.NumericUpDown();
            this.btnTestQuery = new System.Windows.Forms.Button();
            this.chkLogFile = new System.Windows.Forms.CheckBox();
            this.txtLogFile = new System.Windows.Forms.TextBox();
            this.fbdLogFile = new System.Windows.Forms.FolderBrowserDialog();
            this.label18 = new System.Windows.Forms.Label();
            this.chkCustomQuery = new System.Windows.Forms.CheckBox();
            this.btnSaveDBConfig = new System.Windows.Forms.Button();
            this.prgBarIHS = new System.Windows.Forms.ProgressBar();
            this.btnOpen = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.lblStartTime = new System.Windows.Forms.Label();
            this.lblEndTime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRecordSize)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbExtractType
            // 
            this.cmbExtractType.FormattingEnabled = true;
            this.cmbExtractType.Items.AddRange(new object[] {
            "Well",
            "Production Allocated",
            "Production UnAllocated"});
            this.cmbExtractType.Location = new System.Drawing.Point(164, 346);
            this.cmbExtractType.Name = "cmbExtractType";
            this.cmbExtractType.Size = new System.Drawing.Size(262, 28);
            this.cmbExtractType.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 350);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Extract Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(247, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(497, 59);
            this.label2.TabIndex = 1;
            this.label2.Text = "Enerdeq Import Tool";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(34, 411);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 20);
            this.label3.TabIndex = 3;
            this.label3.Text = "State";
            // 
            // cmbState
            // 
            this.cmbState.FormattingEnabled = true;
            this.cmbState.Location = new System.Drawing.Point(164, 406);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(262, 28);
            this.cmbState.TabIndex = 9;
            this.cmbState.SelectedIndexChanged += new System.EventHandler(this.cmbState_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 464);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "County";
            // 
            // cmbCounty
            // 
            this.cmbCounty.FormattingEnabled = true;
            this.cmbCounty.Location = new System.Drawing.Point(164, 459);
            this.cmbCounty.Name = "cmbCounty";
            this.cmbCounty.Size = new System.Drawing.Size(262, 28);
            this.cmbCounty.TabIndex = 10;
            this.cmbCounty.SelectedIndexChanged += new System.EventHandler(this.cmbCounty_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(465, 411);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "State Code";
            // 
            // cmbStateCode
            // 
            this.cmbStateCode.FormattingEnabled = true;
            this.cmbStateCode.Location = new System.Drawing.Point(585, 406);
            this.cmbStateCode.Name = "cmbStateCode";
            this.cmbStateCode.Size = new System.Drawing.Size(107, 28);
            this.cmbStateCode.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(465, 464);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "County Code";
            // 
            // cmbCountyCode
            // 
            this.cmbCountyCode.FormattingEnabled = true;
            this.cmbCountyCode.Location = new System.Drawing.Point(585, 459);
            this.cmbCountyCode.Name = "cmbCountyCode";
            this.cmbCountyCode.Size = new System.Drawing.Size(107, 28);
            this.cmbCountyCode.TabIndex = 12;
            // 
            // chkProductionstatus
            // 
            this.chkProductionstatus.AutoSize = true;
            this.chkProductionstatus.Checked = true;
            this.chkProductionstatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProductionstatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkProductionstatus.Location = new System.Drawing.Point(728, 409);
            this.chkProductionstatus.Name = "chkProductionstatus";
            this.chkProductionstatus.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkProductionstatus.Size = new System.Drawing.Size(294, 24);
            this.chkProductionstatus.TabIndex = 13;
            this.chkProductionstatus.Text = "Current production status Active";
            this.chkProductionstatus.UseVisualStyleBackColor = true;
            // 
            // dtpCompletionDate
            // 
            this.dtpCompletionDate.Location = new System.Drawing.Point(881, 462);
            this.dtpCompletionDate.Name = "dtpCompletionDate";
            this.dtpCompletionDate.Size = new System.Drawing.Size(300, 26);
            this.dtpCompletionDate.TabIndex = 14;
            this.dtpCompletionDate.Value = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(34, 521);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "Hole Direction";
            // 
            // cmbHoleDirection
            // 
            this.cmbHoleDirection.FormattingEnabled = true;
            this.cmbHoleDirection.Items.AddRange(new object[] {
            "HORIZONTAL",
            "DIRECTIONAL"});
            this.cmbHoleDirection.Location = new System.Drawing.Point(164, 517);
            this.cmbHoleDirection.Name = "cmbHoleDirection";
            this.cmbHoleDirection.Size = new System.Drawing.Size(262, 28);
            this.cmbHoleDirection.TabIndex = 15;
            // 
            // btnGetDatatoDB
            // 
            this.btnGetDatatoDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetDatatoDB.Location = new System.Drawing.Point(329, 735);
            this.btnGetDatatoDB.Name = "btnGetDatatoDB";
            this.btnGetDatatoDB.Size = new System.Drawing.Size(415, 91);
            this.btnGetDatatoDB.TabIndex = 20;
            this.btnGetDatatoDB.Text = "&Get IHS Data to DB";
            this.btnGetDatatoDB.UseVisualStyleBackColor = true;
            this.btnGetDatatoDB.Click += new System.EventHandler(this.btnGetDatatoDB_Click);
            // 
            // txtQuery
            // 
            this.txtQuery.Location = new System.Drawing.Point(165, 603);
            this.txtQuery.Multiline = true;
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(1016, 114);
            this.txtQuery.TabIndex = 19;
            this.txtQuery.TabStop = false;
            this.txtQuery.Text = resources.GetString("txtQuery.Text");
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(34, 606);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Query";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtPassword);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txtUserName);
            this.panel1.Location = new System.Drawing.Point(12, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(967, 88);
            this.panel1.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(36, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 20);
            this.label10.TabIndex = 20;
            this.label10.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(178, 45);
            this.txtPassword.MaxLength = 50;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(366, 26);
            this.txtPassword.TabIndex = 1;
            this.txtPassword.Text = "shakThi*()9";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(36, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "UserName";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(178, 13);
            this.txtUserName.MaxLength = 100;
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(768, 26);
            this.txtUserName.TabIndex = 0;
            this.txtUserName.Text = "srinivasap74@yahoo.com";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(728, 465);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 20);
            this.label11.TabIndex = 18;
            this.label11.Text = "Completion Date";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.txtDataUsername);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txtDataServer);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.txtDataPwd);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.txtDataDB);
            this.panel2.Location = new System.Drawing.Point(12, 185);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(967, 150);
            this.panel2.TabIndex = 19;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(36, 111);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(93, 20);
            this.label16.TabIndex = 25;
            this.label16.Text = "UserName";
            // 
            // txtDataUsername
            // 
            this.txtDataUsername.Location = new System.Drawing.Point(178, 108);
            this.txtDataUsername.MaxLength = 100;
            this.txtDataUsername.Name = "txtDataUsername";
            this.txtDataUsername.Size = new System.Drawing.Size(315, 26);
            this.txtDataUsername.TabIndex = 5;
            this.txtDataUsername.Text = "sa";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(36, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "Server";
            // 
            // txtDataServer
            // 
            this.txtDataServer.Location = new System.Drawing.Point(178, 39);
            this.txtDataServer.MaxLength = 100;
            this.txtDataServer.Name = "txtDataServer";
            this.txtDataServer.Size = new System.Drawing.Size(768, 26);
            this.txtDataServer.TabIndex = 3;
            this.txtDataServer.Text = "DESKTOP-2L6A92D\\SQLEXPRESS";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(393, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(337, 32);
            this.label14.TabIndex = 21;
            this.label14.Text = "Database Configuration";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(541, 115);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(86, 20);
            this.label12.TabIndex = 20;
            this.label12.Text = "Password";
            // 
            // txtDataPwd
            // 
            this.txtDataPwd.Location = new System.Drawing.Point(654, 112);
            this.txtDataPwd.MaxLength = 50;
            this.txtDataPwd.Name = "txtDataPwd";
            this.txtDataPwd.PasswordChar = '*';
            this.txtDataPwd.Size = new System.Drawing.Size(292, 26);
            this.txtDataPwd.TabIndex = 6;
            this.txtDataPwd.Text = "tiger*()";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(36, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 20);
            this.label13.TabIndex = 18;
            this.label13.Text = "Database";
            // 
            // txtDataDB
            // 
            this.txtDataDB.Location = new System.Drawing.Point(178, 73);
            this.txtDataDB.MaxLength = 100;
            this.txtDataDB.Name = "txtDataDB";
            this.txtDataDB.Size = new System.Drawing.Size(768, 26);
            this.txtDataDB.TabIndex = 4;
            this.txtDataDB.Text = "RSTest";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(465, 524);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(116, 20);
            this.label17.TabIndex = 27;
            this.label17.Text = "Records Size";
            // 
            // numRecordSize
            // 
            this.numRecordSize.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numRecordSize.Location = new System.Drawing.Point(589, 521);
            this.numRecordSize.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numRecordSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRecordSize.Name = "numRecordSize";
            this.numRecordSize.Size = new System.Drawing.Size(110, 26);
            this.numRecordSize.TabIndex = 16;
            this.numRecordSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // btnTestQuery
            // 
            this.btnTestQuery.Enabled = false;
            this.btnTestQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTestQuery.Location = new System.Drawing.Point(37, 775);
            this.btnTestQuery.Name = "btnTestQuery";
            this.btnTestQuery.Size = new System.Drawing.Size(237, 48);
            this.btnTestQuery.TabIndex = 22;
            this.btnTestQuery.Text = "Test &Custom Query";
            this.btnTestQuery.UseVisualStyleBackColor = true;
            this.btnTestQuery.Click += new System.EventHandler(this.btnTestQuery_Click);
            // 
            // chkLogFile
            // 
            this.chkLogFile.AutoSize = true;
            this.chkLogFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLogFile.Location = new System.Drawing.Point(31, 564);
            this.chkLogFile.Name = "chkLogFile";
            this.chkLogFile.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkLogFile.Size = new System.Drawing.Size(199, 24);
            this.chkLogFile.TabIndex = 17;
            this.chkLogFile.Text = "Enable Tool Logging";
            this.chkLogFile.UseVisualStyleBackColor = true;
            this.chkLogFile.CheckedChanged += new System.EventHandler(this.chkLogFile_CheckedChanged);
            // 
            // txtLogFile
            // 
            this.txtLogFile.Location = new System.Drawing.Point(361, 564);
            this.txtLogFile.MaxLength = 100;
            this.txtLogFile.Name = "txtLogFile";
            this.txtLogFile.Size = new System.Drawing.Size(684, 26);
            this.txtLogFile.TabIndex = 18;
            this.txtLogFile.Text = "D:\\Temp\\";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(253, 567);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 20);
            this.label18.TabIndex = 32;
            this.label18.Text = "Folder Path";
            // 
            // chkCustomQuery
            // 
            this.chkCustomQuery.AutoSize = true;
            this.chkCustomQuery.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCustomQuery.Location = new System.Drawing.Point(43, 732);
            this.chkCustomQuery.Name = "chkCustomQuery";
            this.chkCustomQuery.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkCustomQuery.Size = new System.Drawing.Size(148, 24);
            this.chkCustomQuery.TabIndex = 21;
            this.chkCustomQuery.Text = "Custom Query";
            this.chkCustomQuery.UseVisualStyleBackColor = true;
            this.chkCustomQuery.CheckedChanged += new System.EventHandler(this.chkCustomQuery_CheckedChanged);
            // 
            // btnSaveDBConfig
            // 
            this.btnSaveDBConfig.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSaveDBConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveDBConfig.Location = new System.Drawing.Point(991, 82);
            this.btnSaveDBConfig.Name = "btnSaveDBConfig";
            this.btnSaveDBConfig.Size = new System.Drawing.Size(155, 253);
            this.btnSaveDBConfig.TabIndex = 7;
            this.btnSaveDBConfig.Text = "&Save Config Details";
            this.btnSaveDBConfig.UseVisualStyleBackColor = true;
            this.btnSaveDBConfig.Click += new System.EventHandler(this.btnSaveDBConfig_Click);
            // 
            // prgBarIHS
            // 
            this.prgBarIHS.Location = new System.Drawing.Point(38, 842);
            this.prgBarIHS.Maximum = 700;
            this.prgBarIHS.Name = "prgBarIHS";
            this.prgBarIHS.Size = new System.Drawing.Size(1143, 27);
            this.prgBarIHS.TabIndex = 33;
            // 
            // btnOpen
            // 
            this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpen.Location = new System.Drawing.Point(1051, 546);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(130, 47);
            this.btnOpen.TabIndex = 34;
            this.btnOpen.Text = "&Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Tomato;
            this.label19.Location = new System.Drawing.Point(908, 726);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(241, 32);
            this.label19.TabIndex = 35;
            this.label19.Text = "Processing Time";
            // 
            // lblStartTime
            // 
            this.lblStartTime.AutoSize = true;
            this.lblStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartTime.Location = new System.Drawing.Point(780, 767);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(64, 20);
            this.lblStartTime.TabIndex = 36;
            this.lblStartTime.Text = "Start : ";
            // 
            // lblEndTime
            // 
            this.lblEndTime.AutoSize = true;
            this.lblEndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndTime.Location = new System.Drawing.Point(780, 797);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(56, 20);
            this.lblEndTime.TabIndex = 37;
            this.lblEndTime.Text = "End : ";
            // 
            // EnerdeqMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1193, 877);
            this.Controls.Add(this.lblEndTime);
            this.Controls.Add(this.lblStartTime);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.prgBarIHS);
            this.Controls.Add(this.btnSaveDBConfig);
            this.Controls.Add(this.chkCustomQuery);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.txtLogFile);
            this.Controls.Add(this.chkLogFile);
            this.Controls.Add(this.btnTestQuery);
            this.Controls.Add(this.numRecordSize);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtQuery);
            this.Controls.Add(this.btnGetDatatoDB);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbHoleDirection);
            this.Controls.Add(this.dtpCompletionDate);
            this.Controls.Add(this.chkProductionstatus);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbCountyCode);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbStateCode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbCounty);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbState);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbExtractType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EnerdeqMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EnerdeqMain";
            this.Load += new System.EventHandler(this.EnerdeqMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRecordSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbExtractType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbState;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCounty;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbStateCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCountyCode;
        private System.Windows.Forms.CheckBox chkProductionstatus;
        private System.Windows.Forms.DateTimePicker dtpCompletionDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbHoleDirection;
        private System.Windows.Forms.Button btnGetDatatoDB;
        private System.Windows.Forms.TextBox txtQuery;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDataPwd;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDataDB;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDataUsername;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDataServer;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numRecordSize;
        private System.Windows.Forms.Button btnTestQuery;
        private System.Windows.Forms.CheckBox chkLogFile;
        private System.Windows.Forms.TextBox txtLogFile;
        private System.Windows.Forms.FolderBrowserDialog fbdLogFile;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox chkCustomQuery;
        private System.Windows.Forms.Button btnSaveDBConfig;
        private System.Windows.Forms.ProgressBar prgBarIHS;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblStartTime;
        private System.Windows.Forms.Label lblEndTime;
        private System.Windows.Forms.Timer timer1;
    }
}