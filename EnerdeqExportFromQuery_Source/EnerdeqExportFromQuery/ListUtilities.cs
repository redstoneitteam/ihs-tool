﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    public static class ListUtilities
    {
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> list, int chunkSize)
        {
            var i = 0;
            var chunks = from name in list
                         group name by i++ / chunkSize into part
                         select part.AsEnumerable<T>();
            return chunks;
        }
    }
}
