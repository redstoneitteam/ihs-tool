﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    internal class Options
    {
        [Option('u', "username", Required = true, HelpText = "IHS Enerdeq / Energy Web Services username.")]
        public string Username { get; set; }

        [Option('p', "password", Required = true, HelpText = "IHS Enerdeq / Energy Web Services passowrd.")]
        public string Password { get; set; }

        //[Option('d', "fromdate", Required = false,
        //    HelpText = "Earliest date from which to look for updated data.(formatted as yyyy-MM-dd)")]
        //public string FromDate { get; set; }

        [Option('o', "onelinetemplate", Required = false, HelpText = "The oneline export template to use.")]
        public string OnelineTemplate { get; set; }

        [Option('s', "standardtemplate", Required = false, HelpText = "The standard export template to use.")]
        public string StandardTemplate { get; set; }

        [Option('q', "query", Required = true,
           HelpText = "The saved query name or criteriaXML to use.")]
        public string Query { get; set; }

        [Option('f', "filetype", Required = false, DefaultValue = "Comma Separated",
           HelpText = "The file type to create. One of Comma Separated, Tab Delimited, HTML, Comma Separated, Excel.")]
        public string FileType { get; set; }

        [Option('l', "url", DefaultValue = "https://webservices.ihsenergy.com/WebServices/", HelpText = "The base url for the services to connect to.")]
        public string Url { get; set; }

        [Option('w', "workingDirectory", Required = false, HelpText = "The base directory to download files to.")]
        public string WorkingDirectory { get; set; }

        [Option('c', "chunkSize", Required = false, DefaultValue = 1000, HelpText = "The size of each page / chunk.")]
        public int ChunkSize { get; set; }


        [Option('v', "verbose", DefaultValue = false, HelpText = "Prints all messages to standard output.")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        public Options()
        {
                        WorkingDirectory = Path.GetFullPath(Path.Combine(Path.GetTempPath(), "ExportFromQuerySample", string.Format("{0:yyyy-MM-dd_HH-mm}", DateTime.Now)));

            ChunkSize = 1500;
        }



    }
}
