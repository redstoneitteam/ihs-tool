﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    public static class IHSUtilities
    {

        public static bool WriteLog(string txtData, string filename)
        {
            using (StreamWriter w = File.AppendText(filename))
            {
                w.WriteLine(txtData);
            }
            return true;
        }

        public static string[] ReadLog(string filename)
        {
            try
            {
                string[] configlines = File.ReadAllLines(filename);

                return configlines;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
