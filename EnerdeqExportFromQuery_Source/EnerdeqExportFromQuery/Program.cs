﻿using IHSEnergy.Enerdeq.ExportBuilder;
using IHSEnergy.Enerdeq.QueryBuilder;
using IHSEnergy.Enerdeq.Session;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    class Program
    {
        static void Main2(string[] args)
        {
                
            // Parse commandline parameters
            var options = new Options();
            if (!CommandLine.Parser.Default.ParseArguments(args, options))
            {
                Console.ReadLine();
                return;
            }
            int iExtractType=0;
            
            Console.WriteLine("================================================================================");
            Console.WriteLine("=====================                                     ======================");
            Console.WriteLine("=====================       ENERDEQ EXPORT FROM QUERY      =====================");
            Console.WriteLine("=====================                                     ======================");
            Console.WriteLine("================================================================================\n");

            Console.WriteLine("To Download, Press 1 for Well, 2 for Production, Any other key to Exit \t\t");
            switch  (Console.ReadKey().Key) 
            {
                case ConsoleKey.D1: // Well
                    iExtractType = 1;
                    options.StandardTemplate = "Excel Well Workbook (CSV)"; //"Well Completion List";//"Excel Well Workbook (Excel 2007, 2010)"; // "297 Well (comma delimited)"; //"Excel Well Workbook (CSV)"; // "298 Production (Comma Delimited)"; // "Well ID List"; //"EnerdeqML Well";
                    break;

                case ConsoleKey.D2: // Production
                    iExtractType = 2;
                    options.StandardTemplate = "Excel Production Workbook (CSV)"; //"Well Completion List";//"Excel Well Workbook (Excel 2007, 2010)"; // "297 Well (comma delimited)"; //"Excel Well Workbook (CSV)"; // "298 Production (Comma Delimited)"; // "Well ID List"; //"EnerdeqML Well";
                    break;

                default:
                    return;
            }

            


            Console.WriteLine("\r");

            Console.WriteLine("Enter State <space> County Info to Download \r");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("E.g TX ARCHER \r");
            string strStateCounty = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Enter State Code to Download \r");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("E.g 42 \r");
            string strStateCode = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Enter County Code to Download \r");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("E.g 009 \r");
            string strCountyCode = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Do you wish to continue [Y/N] \n");
            if (Console.ReadKey().Key == ConsoleKey.N || Console.ReadKey().Key == ConsoleKey.Escape) // Production
            {
                return;
            }
            Console.WriteLine("=======================================================");
            
            
            if (string.IsNullOrEmpty(options.OnelineTemplate) && string.IsNullOrEmpty(options.StandardTemplate))
            {
                // default to oneline well header list
                Console.WriteLine("Both Standard and Oneline template missing, being defaulting to 'Well Header List'");
                options.OnelineTemplate = "Well Header List";
            }

            //TODO Add option to read local file for query
            //TODO Add option to read local file for oneline template

            if (options.Verbose)
            {
                Console.WriteLine("URL: {0}", options.Url);
                Console.WriteLine("Username: {0}", options.Username);
                Console.WriteLine("Query: {0}", options.Query);
                Console.WriteLine("Oneline: {0}", options.OnelineTemplate);
                Console.WriteLine("Standard: {0}", options.StandardTemplate);
                Console.WriteLine("Filetype: {0}", options.FileType);
                Console.WriteLine("Working Directory: {0}", options.WorkingDirectory);
                Console.WriteLine();

            }


            var username = options.Username;
            var password = options.Password;
            var onelineTemplate = options.OnelineTemplate;
            var standardTemplate = options.StandardTemplate;
            var query = options.Query;
            var filetype = options.FileType;



            var gtempFilename = "";



            bool buildStandardFlag = !string.IsNullOrEmpty(options.StandardTemplate);

            // Set up working directory
            var workingDirectory = options.WorkingDirectory;
            try
            {
                if (!Directory.Exists(workingDirectory))
                    Directory.CreateDirectory(workingDirectory);
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine("Unable to create working directory. {0}", exception.Message);
                return;
            }

            // override url to hit staging deployment, by default session will be created 
            // against the Internet deployment
            // Production : https://webservices.ihsenergy.com/WebServices/
            // Staging: https://swebservices.ihsenergy.com/WebServices/

            var url = options.Url;

            using (var session = WebServicesSession.Create(url, username, password, "redstone_wellprodact_ingest")) //EnerdeqExportFromQuery
            {
                string entitlements = session.GetEntitlements();

                using (var queryBuilder = new QueryBuilder(session))
                {
                    // Get a list of Ids for the provided query
                    var count = queryBuilder.GetCount(query);
                    if (options.Verbose) Console.WriteLine("Query results in {0} items. ", count);

                    using (var exportBuilder = new ExportBuilder(session))
                    {
                       

                        var datatype = EwsUtils.GetDatatypeFromQuery(queryBuilder, query);

                        string[] templates = exportBuilder.GetTemplates("US", datatype);
                        foreach (string template in templates)
                            Console.WriteLine(template);

                        Console.ReadKey();

                        //// If count is over 5000 use the ExportBuilder to get the id list
                        //string[] ids = count <= 5000 ? queryBuilder.GetKeys(query) : EwsUtils.GetIdList(exportBuilder, datatype, query, options.Verbose);

                        //// Chunk the list of ids to something reasonable
                        //int chunkSize = options.ChunkSize;  // best performance < 2000

                        //var chunks = ids.Chunk(chunkSize);




                        //foreach (var chunk in chunks)
                        //{
                            var filename = string.Format("{0}", Guid.NewGuid());

                            string jobid;
                        //if (buildStandardFlag)
                        //{
                        //    // Build standard export
                        //    jobid = exportBuilder.Build("US", datatype, standardTemplate, chunk.ToArray(), filename, Overwrite.True);
                        //}
                        //else
                        //{
                        //    // Build oneline export
                        //    jobid = exportBuilder.BuildOneline("US", datatype, chunk.ToArray(), onelineTemplate, filetype, filename, Overwrite.True);
                        //}


                        //string xml = "<criterias><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Location</attribute_group><attribute>State/County</attribute><filter logic=\"include\"><value id=\"0\" ignored=\"false\"><group_actual><operator logic=\"and\"> <condition logic=\"equals\"> <attribute>state_code</attribute><value_list> <value>35</value></value_list></condition></operator></group_actual><group_display>name starts with OK</group_display></value></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Well</attribute_group><attribute>Current Production Status</attribute><filter logic=\"include\"><value id=\"1\" ignored=\"false\"><group_actual><operator logic=\"and\"> <condition logic=\"equals\"> <attribute>code</attribute><value_list> <value>A</value></value_list></condition></operator></group_actual><group_display>name = ACTIVE</group_display></value></filter></criteria><criteria type=\"value\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Date</attribute_group><attribute>Completion Date</attribute><type>date</type><displaytype /><filter logic=\"greater_than_or_equals\"><value id=\"2\" ignored=\"false\" display=\"after 1 / 1 / 2010\" actual=\"2010 / 1 / 1 00:00:00\" /></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Well</attribute_group><attribute>Hole Direction</attribute><filter logic=\"include\"><value id=\"3\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>H</value></value_list></condition></operator></group_actual><group_display>name = HORIZONTAL</group_display></value><value id=\"4\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>D</value></value_list></condition></operator></group_actual><group_display>name = DIRECTIONAL</group_display></value></filter></criteria></criterias>";

                        //TX ANGELINA -- 42 --005
                        string xml = "<criterias><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Location</attribute_group><attribute>State/County</attribute><filter logic=\"include\"><value id=\"0\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>state_code</attribute><value_list><value>" + strStateCode + "</value></value_list></condition><condition logic=\"equals\"><attribute>county_code</attribute><value_list><value>" + strCountyCode + "</value></value_list></condition></operator></group_actual><group_display>name = " + strStateCounty + "</group_display></value></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Well</attribute_group><attribute>Current Production Status</attribute><filter logic=\"include\"><value id=\"1\" ignored=\"false\"><group_actual><operator logic=\"and\"> <condition logic=\"equals\"> <attribute>code</attribute><value_list> <value>A</value></value_list></condition></operator></group_actual><group_display>name = ACTIVE</group_display></value></filter></criteria><criteria type=\"value\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Date</attribute_group><attribute>Completion Date</attribute><type>date</type><displaytype /><filter logic=\"greater_than_or_equals\"><value id=\"2\" ignored=\"false\" display=\"after 1 / 1 / 2010\" actual=\"2010 / 1 / 1 00:00:00\" /></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>Well</datatype><attribute_group>Well</attribute_group><attribute>Hole Direction</attribute><filter logic=\"include\"><value id=\"3\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>H</value></value_list></condition></operator></group_actual><group_display>name = HORIZONTAL</group_display></value><value id=\"4\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>D</value></value_list></condition></operator></group_actual><group_display>name = DIRECTIONAL</group_display></value></filter></criteria></criterias>";

                            jobid = exportBuilder.BuildFromQuery("US", datatype, standardTemplate, xml, filename, Overwrite.True);

                            // Store file in temp directory
                            var tempFilename = Path.Combine(workingDirectory, jobid);
                            gtempFilename = tempFilename; //copy to global variable
                                                          //EwsUtils.RetrieveFile(exportBuilder, jobid, tempFilename, options.Verbose);

                        //uncomment to use it
                        //DataSet dsRSRData =  EwsUtils.RetrieveDataSet(exportBuilder, jobid, tempFilename, options.Verbose, iExtractType);

                        //uncomment to use it
                        ////Write to SQL DB
                        //uploadtoDB(dsRSRData, iExtractType);

                        if (options.Verbose) Console.Out.WriteLine("File '{0}' created. ", tempFilename);
                        //}
                    }

                }

            }

            if (options.Verbose)
            {
                Console.WriteLine("Completed. Press any key to continue.");
                Console.ReadLine();
            }

            

            //Write to SQL DB
            //uploadtoSQL(gtempFilename);

        }

        private static void uploadtoDB(DataSet dsRSRData,int iExtractType)
        {
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-2L6A92D\SQLEXPRESS;Initial Catalog=RSTest;Persist Security Info=True;User ID=sa;Password = tiger*()"); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
            con.Open();
            foreach (DataTable dt in dsRSRData.Tables)
            {
                //foreach (DataColumn dc in dt.Columns)
                //{
                //    Console.WriteLine("Table Name : " + dt.TableName + " Column Name : " + dc.ColumnName); 
                //}
                string tblName = string.Empty;
                if (iExtractType == 1) //Well
                {

                    //Well
                    if (dt.TableName.Contains("Header"))
                    {
                        tblName = "Header";
                    }
                    if (dt.TableName.Contains("Location"))
                    {
                        tblName = "Location";
                    }
                    if (dt.TableName.Contains("TreatmentSummary"))
                    {
                        tblName = "TreatmentSummary";
                    }
                }

                if (iExtractType == 2) //Production
                {

                    //Production
                    if (dt.TableName.Contains("ProductionHeader"))
                    {
                        tblName = "ProductionHeader";
                    }
                    if (dt.TableName.Contains("ProductionWell"))
                    {
                        tblName = "ProductionWell";
                    }
                    if (dt.TableName.Contains("ProductionAbstract"))
                    {
                        tblName = "ProductionAbstract";
                    }
                    if (dt.TableName.Contains("MonthlyProduction"))
                    {
                        tblName = "MonthlyProduction";
                    }
                    if (dt.TableName.Contains("IPCumNormValues"))
                    {
                        tblName = "IPCumNormValues";
                    }
                }
                //if (dt.TableName.Contains("Completion"))
                //{
                //    tblName = "Completion";
                //}

                if (tblName != string.Empty)
                {
                    SqlBulkCopy bc = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock);
                    bc.DestinationTableName = tblName; // "tblRSRExports";
                    bc.BatchSize = dt.Rows.Count; //dsRSRData.Tables["tblTest"].Rows.Count;               
                    bc.WriteToServer(dt);
                    bc.Close();
                }

            }
            con.Close();
        }

        private static void uploadtoSQL(string gtempFilename)
        {
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-2L6A92D\SQLEXPRESS;Initial Catalog=RSTest;Persist Security Info=True;User ID=sa;Password = tiger*()"); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");


            string filepath = gtempFilename; // @"C:\Users\RSR\AppData\Local\Temp\ExportFromQuerySample\2018-04-06_19-15\exports\bd80c3cf-288f-4ca9-923d-c5f398c664d9.csv"; //  "C:\\params.csv";
            StreamReader sr = new StreamReader(filepath);
            string line = sr.ReadLine();
            string[] value = line.Split(',');
            DataTable dt = new DataTable();
            DataRow row;
            foreach (string dc in value)
            {
                dt.Columns.Add(new DataColumn(dc));
            }

            while (!sr.EndOfStream)
            {
                value = sr.ReadLine().Split(',');
                if (value.Length == dt.Columns.Count)
                {
                    row = dt.NewRow();
                    row.ItemArray = value;
                    dt.Rows.Add(row);
                }
            }
            SqlBulkCopy bc = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock);
            bc.DestinationTableName = "tblRSRExports";
            bc.BatchSize = dt.Rows.Count;
            con.Open();
            bc.WriteToServer(dt);
            bc.Close();
            con.Close();
        }
       
    }
}

