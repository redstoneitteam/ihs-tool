﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.FileIO;
using IHSEnergy.Enerdeq.ExportBuilder;
using IHSEnergy.Enerdeq.QueryBuilder;
using IHSEnergy.Enerdeq.Session;
using IHSEnergy.Enerdeq.DataTemplateManager;
using System.IO;
using System.Xml;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    public partial class EnerdeqMain : Form
    {
        public int gChunksCount = 0;
        public static bool result=false;
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new EnerdeqMain());

            

        }
        public EnerdeqMain()
        {
            InitializeComponent();
        }

        private void GetDataTableFromCSVFile(string csvfilePath)
        {
          
            using (TextFieldParser csvReader = new TextFieldParser(csvfilePath))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;

                //Read columns from CSV file, remove this line if columns not exits  
                string[] colFields = csvReader.ReadFields(); //ignore headers

                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    //Making empty value as null
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }                        
                    }
                    if (!cmbState.Items.Contains(fieldData[0]))
                        cmbState.Items.Add(fieldData[0]);

                    if (!cmbStateCode.Items.Contains(fieldData[1]))
                        cmbStateCode.Items.Add(fieldData[1]);

                    cmbCounty.Items.Add(fieldData[2]);
                    cmbCountyCode.Items.Add(fieldData[3]);
                    //csvData.Rows.Add(fieldData);
                }
            }
           
        }

        private void GetDataTableFromCSVFileForState(string csvfilePath, string strState)
        {

            using (TextFieldParser csvReader = new TextFieldParser(csvfilePath))
            {
                csvReader.SetDelimiters(new string[] { "," });
                csvReader.HasFieldsEnclosedInQuotes = true;

                //Read columns from CSV file, remove this line if columns not exits  
                string[] colFields = csvReader.ReadFields(); //ignore headers
                
                cmbCounty.Items.Clear();
                cmbCountyCode.Items.Clear();
                cmbCounty.Items.Add("ALL");
                cmbCountyCode.Items.Add("00");
                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    //Making empty value as null
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }
                    }
                    if ((!cmbCounty.Items.Contains(fieldData[0])) && (fieldData[0]==strState))
                    {
                        cmbCounty.Items.Add(fieldData[2]);
                        cmbCountyCode.Items.Add(fieldData[3]);
                    }
                }
            }

        }

        private void EnerdeqMain_Load(object sender, EventArgs e)
        {
            try
            {
                string appStartUp = Application.StartupPath;
                GetDataTableFromCSVFile(appStartUp + "\\Config\\enerdeq.csv");
                cmbExtractType.SelectedIndex = 0;
                cmbHoleDirection.SelectedIndex = 1;
                txtLogFile.Text = Application.StartupPath + "\\Log\\";

                //Read Config details
                string[] configlines = IHSUtilities.ReadLog(appStartUp + "\\Config\\config.csv");
                for (int iCnt=0; iCnt<configlines.Length;iCnt++)
                {
                    if (iCnt==0) txtUserName.Text = configlines[iCnt];
                    if (iCnt == 1) txtPassword.Text = configlines[iCnt];
                    if (iCnt == 2) txtDataServer.Text = configlines[iCnt];
                    if (iCnt == 3) txtDataDB.Text = configlines[iCnt];
                    if (iCnt == 4) txtDataUsername.Text = configlines[iCnt];
                    if (iCnt == 5) txtDataPwd.Text = configlines[iCnt];
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Could not find file"))
                {
                    MessageBox.Show("Please create the file by saving config", "Enerdeq Tool", MessageBoxButtons.OK);
                }
            }

        }

        private void cmbCounty_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbCountyCode.SelectedIndex = cmbCounty.SelectedIndex;
        }

        private void cmbState_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbStateCode.SelectedIndex = cmbState.SelectedIndex;
            string appStartUp = Application.StartupPath;
            GetDataTableFromCSVFileForState(appStartUp + "\\Config\\enerdeq.csv", cmbState.Text);
        }

        private string ConcatenateCheckBoxListItems(ComboBox cmbControl)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < cmbControl.Items.Count; ++i)
            {
                if (cmbControl.GetItemText(cmbControl.Items[i]) != "ALL")
                {
                    if (cmbControl.GetItemText(cmbControl.Items[i]) != "00")
                    {
                        sb.Append("<Value>" + cmbControl.GetItemText(cmbControl.Items[i]) + "</Value>");
                    }
                }
            }

            return sb.ToString();
        }

        private void btnGetDatatoDB_Click(object sender, EventArgs e)
        {
            try
            {
               
               
                if (!txtLogFile.Text.Contains("Log"))
                {
                    txtLogFile.Text = txtLogFile.Text + "Log" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace("-", "").Replace(" ", "") + ".txt";
                }
                else
                {
                    txtLogFile.Text=txtLogFile.Text.Remove(txtLogFile.Text.IndexOf("Log"), txtLogFile.Text.Length- txtLogFile.Text.IndexOf("Log"));
                    txtLogFile.Text = txtLogFile.Text + "Log" + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace("-", "").Replace(" ", "") + ".txt";
                }

                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Log Begins -------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                //prgBarIHS.Value = 5;
                var username = txtUserName.Text;
                var password = txtPassword.Text;
                //var onelineTemplate = "Well Header List";
                var standardTemplate = string.Empty;

                int iExtractType = 0;
                if (cmbExtractType.Text == "Well")
                {
                    iExtractType = 1;
                    standardTemplate = "Excel Well Workbook (CSV)";
                }

                if (cmbExtractType.Text.StartsWith("Production"))
                {
                    iExtractType = 2;
                    standardTemplate = "Excel Production Workbook (CSV)";
                }

                string strState = ConcatenateCheckBoxListItems(cmbState);
                if (cmbState.Text != "ALL")
                {
                    if (cmbState.Text.Length == 0)
                    {
                        MessageBox.Show("Please select State", "Enerdeq Tool", MessageBoxButtons.OK);
                        return;
                    }
                    else
                    strState = cmbState.Text;
                }

                string strStateCode = ConcatenateCheckBoxListItems(cmbStateCode);
                if (cmbStateCode.Text != "00")
                {
                    if (cmbStateCode.Text.Length == 0)
                    {
                        MessageBox.Show("Please select State Code", "Enerdeq Tool", MessageBoxButtons.OK);
                        return;
                    }
                    else
                        strStateCode = cmbStateCode.Text;
                }

                string strCounty = ConcatenateCheckBoxListItems(cmbCounty);
                if (cmbCounty.Text != "ALL")
                {
                    if (cmbCounty.Text.Length == 0)
                    {
                        MessageBox.Show("Please select County", "Enerdeq Tool", MessageBoxButtons.OK);
                        return;
                    }
                    else
                        strCounty = cmbCounty.Text;
                }

                string strCountyCode = ConcatenateCheckBoxListItems(cmbCountyCode);
                if (cmbCountyCode.Text != "00")
                {
                    if (cmbCountyCode.Text.Length == 0)
                    {
                        MessageBox.Show("Please select County Code", "Enerdeq Tool", MessageBoxButtons.OK);
                        return;
                    }
                    else
                        strCountyCode = cmbCountyCode.Text;
                }


                string strStateCounty = cmbState.Text + " " + cmbCounty.Text;

                string strHoleDirection = cmbHoleDirection.Text;
                string strCompletionDate = dtpCompletionDate.Value.Day.ToString() + " / " + dtpCompletionDate.Value.Month.ToString() + " / " + dtpCompletionDate.Value.Year.ToString();
                string revStrCompletionDate = dtpCompletionDate.Value.Year.ToString() + " / " + dtpCompletionDate.Value.Month.ToString() + " / " + dtpCompletionDate.Value.Day.ToString();



                string strServer = txtDataServer.Text;
                string strDatabase = txtDataDB.Text;
                string strUname = txtDataUsername.Text;
                string strPwd = txtDataPwd.Text;

                string strCurProdStatus = chkProductionstatus.Checked ? "ACTIVE" : "INACTIVE";

                var query = txtQuery.Text;
                var filetype = "Comma Separated";
                var gtempFilename = "";

                bool buildStandardFlag = !string.IsNullOrEmpty(standardTemplate);

                // override url to hit staging deployment, by default session will be created 
                // against the Internet deployment
                // Production : https://webservices.ihsenergy.com/WebServices/
                // Staging: https://swebservices.ihsenergy.com/WebServices/

                var url = "https://webservices.ihsenergy.com/WebServices/";

                Cursor.Current = Cursors.WaitCursor;
                lblStartTime.Text = "Start : ";
                lblEndTime.Text = "End : ";
                using (var session = WebServicesSession.Create(url, username, password, "redstone_wellprodact_ingest")) //EnerdeqExportFromQuery
                {
                    if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Session Created -------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                    string entitlements = session.GetEntitlements();

                    using (var queryBuilder = new QueryBuilder(session))
                    {
                        if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Query Builder Created -------------------" + DateTime.Now.ToString(), txtLogFile.Text);


                        using (var exportBuilder = new ExportBuilder(session))
                        {
                            string[] domains = exportBuilder.GetDomains();

                            var filename = string.Format("{0}", Guid.NewGuid());

                            string jobid;

                            //TX ANGELINA -- 42 --005
                            //string xml = "<criterias><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>Location</attribute_group><attribute>State/County</attribute><filter logic=\"include\"><value id=\"0\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>state_code</attribute><value_list><value>" + strStateCode + "</value></value_list></condition><condition logic=\"equals\"><attribute>county_code</attribute><value_list><value>" + strCountyCode + "</value></value_list></condition></operator></group_actual><group_display>name = " + strStateCounty + "</group_display></value></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>" + cmbExtractType.Text + "</attribute_group><attribute>Current Production Status</attribute><filter logic=\"include\"><value id=\"1\" ignored=\"false\"><group_actual><operator logic=\"and\"> <condition logic=\"equals\"> <attribute>code</attribute><value_list> <value>A</value></value_list></condition></operator></group_actual><group_display>name = " + strCurProdStatus + "</group_display></value></filter></criteria><criteria type=\"value\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>Date</attribute_group><attribute>Completion Date</attribute><type>date</type><displaytype /><filter logic=\"greater_than_or_equals\"><value id=\"2\" ignored=\"false\" display=\"after "  + strCompletionDate + "\" actual=\"" + revStrCompletionDate + " 00:00:00\" /></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>" + cmbExtractType.Text + "</attribute_group><attribute>Hole Direction</attribute><filter logic=\"include\"><value id=\"3\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>H</value></value_list></condition></operator></group_actual><group_display>name = " + strHoleDirection + "</group_display></value><value id=\"4\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>D</value></value_list></condition></operator></group_actual><group_display>name = " + strHoleDirection + "</group_display></value></filter></criteria></criterias>";
                            string strCurrProdCode = string.Empty;

                            if (strCurProdStatus == "ACTIVE")
                                strCurrProdCode = "A";
                            else
                                strCurrProdCode = "I";

                            string strHoleDirectionCode = string.Empty;
                            if (strHoleDirection == "HORIZONTAL")
                                strHoleDirectionCode = "H";
                            else
                                strHoleDirectionCode = "D";

                            if (strHoleDirection == "VERTICAL")
                                strHoleDirectionCode = "V";

                            string xml = string.Empty;

                            xml = ToXml(strCompletionDate, revStrCompletionDate);
                            txtQuery.Text = xml;


                            if (lblStartTime.Text == "Start : ")
                                lblStartTime.Text = lblStartTime.Text + DateTime.Now.ToString();
                            else
                                lblStartTime.Text = "Start : ";

                            if (MessageBox.Show("XML is ready; Do you wish to continue with data import", "Enerdeq Tool", MessageBoxButtons.YesNo) == DialogResult.No)
                            {
                                lblEndTime.Text = lblEndTime.Text + DateTime.Now.ToString();
                                return;
                            }
                            // Get a list of Ids for the provided query
                            var count = queryBuilder.GetCount(xml);
                            //if (options.Verbose) Console.WriteLine("Query results in {0} items. ", count);//

                            MessageBox.Show("Total Count of Records Fetched : " + count.ToString(), "Enerdeq Tool", MessageBoxButtons.OK);

                            if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Count of Total Records Matched : " + count.ToString() + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);


                            // If count is over 5000 use the ExportBuilder to get the id list
                            string[] ids = count <= 5000 ? queryBuilder.GetKeys(xml) : EwsUtils.GetIdList(exportBuilder, cmbExtractType.Text, xml, false);
                            prgBarIHS.Maximum = 100;
                            prgBarIHS.Value = 5;

                            Task.Delay(1000).Wait();
                            
                            // Chunk the list of ids to something reasonable
                            int chunkSize = Convert.ToUInt16(numRecordSize.Value); // options.ChunkSize;  // best performance < 2000
                            gChunksCount = chunkSize;
                            //var chunks = retIds.Chunk(chunkSize);
                            var chunks = ids.Chunk(chunkSize);

                            prgBarIHS.Value = 15;

                            Task.Delay(1000).Wait();
                            prgBarIHS.Maximum = prgBarIHS.Maximum+(int)(count/5);
                            
                            int iCnt = 1;
                            foreach (var chunk in chunks)
                            {
                                jobid = exportBuilder.Build("US", cmbExtractType.Text, standardTemplate, chunk.ToArray(), filename, Overwrite.True);
                                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Export Builder Job ID : " + jobid + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                                
                                if (prgBarIHS.Value + 5 < prgBarIHS.Maximum)
                                {
                                    prgBarIHS.Value += 5;
                                }

                                Task.Delay(1000).Wait();
                                
                                //Currently working  - For Reference
                                //jobid = exportBuilder.BuildFromQuery("US", cmbExtractType.Text, standardTemplate, xml, filename, Overwrite.True);
                                ////jobid = exportBuilder.BuildOnelineFromQuery(xml, standardTemplate, "Comma Separated", filename, Overwrite.True);

                                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Data retreiving begins" + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                                
                                DataSet dsRSRData = EwsUtils.RetrieveDataSet(exportBuilder, jobid, txtLogFile.Text, false, iExtractType, strServer, strDatabase, strUname, strPwd,chkLogFile.Checked);
                                
                                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Data retreiving ends " + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                                if (dsRSRData != null)
                                {
                                    if (dsRSRData.Tables.Count > 0)
                                    {
                                        if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Uploading to Database Begins" + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                                        ////Write to SQL DB
                                        EnerdeqServices.uploadtoDB(dsRSRData, iExtractType, strServer, strDatabase, strUname, strPwd, ids);
                                        if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Uploading to Database Ends" + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                                    }
                                }

                                if (chkLogFile.Checked)
                                {
                                    if (chunkSize < numRecordSize.Value)
                                        result = IHSUtilities.WriteLog("-------------- " + "Uploaded " + count.ToString() + " records to Database " + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                                    else
                                        result = IHSUtilities.WriteLog("-------------- " + "Uploaded " + (chunkSize * iCnt).ToString() + " records to Database " + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                                }
                                iCnt++;
                                //Not required -- Do you wish to continue message as decided on April 4
                                ////if ((chunkSize * iCnt) < count)
                                ////{
                                ////    if (MessageBox.Show("Do you wish to continue..", "Enerdeq Tool", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                ////    {

                                ////        //if (prgBarIHS.Value < prgBarIHS.Maximum)
                                ////        //{
                                ////        //    prgBarIHS.Value = prgBarIHS.Value + ((prgBarIHS.Maximum - prgBarIHS.Value) / iCnt);
                                ////        //}

                                ////        continue;

                                ////    }
                                ////    else
                                ////        break;
                                ////}
                                
                            }

                            if (lblEndTime.Text == "End : ")
                                lblEndTime.Text = lblEndTime.Text + DateTime.Now.ToString();
                            else
                                lblEndTime.Text = "End : ";


                            prgBarIHS.Value = prgBarIHS.Maximum;
                            if (iCnt > 1 || chunkSize<= numRecordSize.Value)
                            {
                               MessageBox.Show("Data Imported Successfully", "Enerdeq Tool", MessageBoxButtons.OK);
                            }
                            else
                            {
                                MessageBox.Show("No Data Imported", "Enerdeq Tool", MessageBoxButtons.OK);
                            }
                            Cursor.Current = Cursors.Default;
                            //prgBarIHS.Value = prgBarIHS.Maximum;
                            if (chkLogFile.Checked)
                            {
                                result = IHSUtilities.WriteLog("-------------- " + "Data Importing Ends Successfully" + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Log Ends -------------------" + DateTime.Now.ToString(), txtLogFile.Text);

                            }

                        }

                    }
                

                }
            }
            catch (Exception ex)
            {
                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- " + "Error while Data Importing data to table : " + EnerdeqServices.tblName + " and Message is :: " + ex.Message + "-------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                if (chkLogFile.Checked) result = IHSUtilities.WriteLog("-------------- Log Ends -------------------" + DateTime.Now.ToString(), txtLogFile.Text);
                MessageBox.Show("Data Import Failed : "  +ex.Message, "Enerdeq Tool", MessageBoxButtons.OK);
                prgBarIHS.Value = prgBarIHS.Maximum;
                Cursor.Current = Cursors.Default;
                timer1.Stop();
            }

        }

        public string ToXml(string strCompletionDate,string revStrCompletionDate)
        {
            StringBuilder builder = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(builder))
            {
                using (XmlTextWriter writer = new XmlTextWriter(stringWriter))
                {

                    //xml = "<criterias><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain>
                    //<datatype>" + cmbExtractType.Text + "</datatype><attribute_group>Well</attribute_group>
                    //<attribute>Hole Direction</attribute>
                    //<filter logic=\"include\"><value id=\"0\" ignored=\"false\">
                    //<group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute>
                    //<value_list><value>" + strHoleDirectionCode + "</value></value_list></condition></operator>
                    //</group_actual><group_display>name = " + strHoleDirection + "</group_display></value></filter>
                    //</criteria>
                    //<criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>Production</attribute_group><attribute>Production Status</attribute><filter logic=\"include\"><value id=\"1\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>code</attribute><value_list><value>" + strCurrProdCode + "</value></value_list></condition></operator></group_actual><group_display>name = " + strCurProdStatus + "</group_display></value></filter></criteria><criteria type=\"group\" groupId=\"\" ignored=\"false\"><domain>US</domain><datatype>" + cmbExtractType.Text + "</datatype><attribute_group>Location</attribute_group><attribute>State/County</attribute><filter logic=\"include\"><value id=\"2\" ignored=\"false\"><group_actual><operator logic=\"and\"><condition logic=\"equals\"><attribute>state_code</attribute><value_list><value>" + strStateCode + "</value></value_list></condition><condition logic=\"equals\"><attribute>county_code</attribute><value_list><value>" + strCountyCode + "</value></value_list></condition></operator></group_actual><group_display>name = " + strStateCounty + "</group_display></value></filter></criteria><criteria type=\"value\" ignored=\"false\"><domain>US</domain><datatype>Production Allocated</datatype><attribute_group>Date</attribute_group><attribute>Production Start Date</attribute><type>date</type><displaytype/><filter logic=\"greater_than_or_equals\"><value id=\"3\" actual=\"" + revStrCompletionDate + " 00:00:00\"   display=\"after " + strCompletionDate + "\" ignored=\"false\"/></filter></criteria></criterias>";


                    // This produces UTF16 XML
                    //writer.Indentation = 4;
                    //writer.IndentChar = '\t';
                    //writer.Formatting = Formatting.Indented;
                    //writer.WriteStartDocument();
                    writer.WriteStartElement("criterias");
                    writer.WriteStartElement("criteria", "");
                    writer.WriteAttributeString("type", "group");
                    writer.WriteAttributeString("groupId", "");
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteElementString("domain", "US");
                    writer.WriteElementString("datatype", cmbExtractType.Text);
                    writer.WriteElementString("attribute_group", "Location");
                    writer.WriteElementString("attribute", "State/County");
                    writer.WriteStartElement("filter", "");
                    writer.WriteAttributeString("logic", "include");

                    int lastIndex = 0;

                    for (int indexInteger = 0; indexInteger < cmbCounty.Items.Count; indexInteger++)
                    {
                        if (!cmbCounty.Items[indexInteger].ToString().Contains("ALL"))
                        {
                            writer.WriteStartElement("value", "");
                            writer.WriteAttributeString("id", (indexInteger-1).ToString());
                            writer.WriteAttributeString("ignored", "false");
                            writer.WriteStartElement("group_actual", "");
                            writer.WriteStartElement("operator", "");
                            writer.WriteAttributeString("logic", "and");
                            writer.WriteStartElement("condition", "");
                            writer.WriteAttributeString("logic", "equals");
                            writer.WriteElementString("attribute", "state_code");
                            writer.WriteStartElement("value_list", "");
                            writer.WriteElementString("value", cmbStateCode.Text);
                            writer.WriteEndElement();
                            writer.WriteEndElement();

                            writer.WriteStartElement("condition", "");
                            writer.WriteAttributeString("logic", "equals");
                            writer.WriteElementString("attribute", "county_code");
                            writer.WriteStartElement("value_list", "");
                            writer.WriteElementString("value", cmbCountyCode.Items[indexInteger].ToString());
                            writer.WriteEndElement();
                            writer.WriteEndElement();

                            writer.WriteEndElement();
                            writer.WriteEndElement();
                            writer.WriteElementString("group_display", "name = " + cmbState.Text + " " + cmbCounty.Items[indexInteger].ToString());
                            writer.WriteEndElement();
                        }
                        lastIndex = indexInteger;
                }
                    //writer.WriteStartElement("value", "");
                    //writer.WriteAttributeString("id", "1");
                    //writer.WriteAttributeString("ignored", "false");
                    //writer.WriteStartElement("group_actual", "");
                    //writer.WriteStartElement("operator", "");
                    //writer.WriteAttributeString("logic", "and");

                    //writer.WriteStartElement("condition", "");
                    //writer.WriteAttributeString("logic", "equals");
                    //writer.WriteElementString("attribute", "code");
                    //writer.WriteStartElement("value_list", "");
                    //writer.WriteElementString("value", "strHoleDirectionCode");
                    //writer.WriteEndElement();
                    //writer.WriteEndElement();
                    //writer.WriteEndElement();
                    //writer.WriteEndElement();
                    //writer.WriteElementString("group_display", "name = OK CHOCTAW");
                    //writer.WriteEndElement();


                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    //writer.WriteEndDocument();

                    //Current Production

                    writer.WriteStartElement("criteria", "");
                    writer.WriteAttributeString("type", "group");
                    writer.WriteAttributeString("groupId", "");
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteElementString("domain", "US");
                    writer.WriteElementString("datatype", cmbExtractType.Text);
                    writer.WriteElementString("attribute_group", cmbExtractType.Text.StartsWith("Production") ? "Production" : "Well");
                    
                    writer.WriteElementString("attribute", cmbExtractType.Text.StartsWith("Production")?"Production Status" : "Current Production Status");
                    writer.WriteStartElement("filter", "");
                    writer.WriteAttributeString("logic", "include");



                    writer.WriteStartElement("value", "");
                    writer.WriteAttributeString("id", lastIndex.ToString());
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteStartElement("group_actual", "");
                    writer.WriteStartElement("operator", "");
                    writer.WriteAttributeString("logic", "and");
                    writer.WriteStartElement("condition", "");
                    writer.WriteAttributeString("logic", "equals");
                    writer.WriteElementString("attribute", "code");
                    writer.WriteStartElement("value_list", "");
                    writer.WriteElementString("value", chkProductionstatus.Checked ? "A" : "I");
                    writer.WriteEndElement();
                    writer.WriteEndElement();


                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteElementString("group_display", "name = " + (chkProductionstatus.Checked ? "ACTIVE" : "INACTIVE"));
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    lastIndex++;
                    //Current Production


                    //Completion Date

                    writer.WriteStartElement("criteria", "");
                    writer.WriteAttributeString("type", "group");
                    writer.WriteAttributeString("groupId", "");
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteElementString("domain", "US");
                    writer.WriteElementString("datatype", cmbExtractType.Text);
                    writer.WriteElementString("attribute_group", "Date");
                    writer.WriteElementString("attribute",cmbExtractType.Text.StartsWith("Production")? "Production Start Date": "Completion Date");
                    writer.WriteElementString("type", "date");
                    writer.WriteElementString("displaytype", "");
                    writer.WriteStartElement("filter", "");
                    writer.WriteAttributeString("logic", "greater_than_or_equals");



                    writer.WriteStartElement("value", "");
                    writer.WriteAttributeString("id", lastIndex.ToString());
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteAttributeString("display", "after " + strCompletionDate); 

                    writer.WriteAttributeString("actual", revStrCompletionDate + " 00:00:00"); 

                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    lastIndex++;
                    //Completion Date


                    //Hole Direction

                    writer.WriteStartElement("criteria", "");
                    writer.WriteAttributeString("type", "group");
                    writer.WriteAttributeString("groupId", "");
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteElementString("domain", "US");
                    writer.WriteElementString("datatype", cmbExtractType.Text);
                    writer.WriteElementString("attribute_group", "Well");
                    writer.WriteElementString("attribute", "Hole Direction");
                    writer.WriteStartElement("filter", "");
                    writer.WriteAttributeString("logic", "include");



                    writer.WriteStartElement("value", "");
                    writer.WriteAttributeString("id", lastIndex.ToString());
                    writer.WriteAttributeString("ignored", "false");
                    writer.WriteStartElement("group_actual", "");
                    writer.WriteStartElement("operator", "");
                    writer.WriteAttributeString("logic", "and");
                    writer.WriteStartElement("condition", "");
                    writer.WriteAttributeString("logic", "equals");
                    writer.WriteElementString("attribute", "code");
                    writer.WriteStartElement("value_list", "");
                    writer.WriteElementString("value", cmbHoleDirection.Text=="HORIZONTAL" ? "H" : "D");
                    writer.WriteEndElement();
                    writer.WriteEndElement();


                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteElementString("group_display", "name = " + cmbHoleDirection.Text);
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    //Hole Direction
                }
            }
            return builder.ToString();
        }

        //private string BuildCriteriaStateCountyXML()
        //{
        //    String.Join(Environment.NewLine, cmbStateCode.Items.Cast<String>())
        //}

        private void btnTestQuery_Click(object sender, EventArgs e)
        {

            if (txtQuery.Text.Length == 0)
            {
                MessageBox.Show("Please fill the Query");
                txtQuery.Focus();
                return;
            }

            string domain = "US";
            string datatype = cmbExtractType.Text;
            string[] ids = { "42103358130000" };
            string template = "Excel Well Workbook (CSV)"; // "Well Header List"; //A Standard or Custom Oneline template name.
                                                           //    string file = "d:\\Work\\sample2.xlsx";

           
            string filetype = "Excel";
            Overwrite ow = Overwrite.True;

            string url = "https://webservices.ihsenergy.com/WebServices/";
            string user = txtUserName.Text;
            string pwd = txtPassword.Text;
            string _app = "redstone_wellprodact_ingest";

            WebServicesSession ws;
            if (url.Length > 0)
                ws = WebServicesSession.Create(url, user, pwd, _app);
            else
                ws = WebServicesSession.Create(user, pwd, _app);

            string jobId;
            using (var queryBuilder = new QueryBuilder(ws))
            {
                var count = queryBuilder.GetCount(txtQuery.Text);
                //if (options.Verbose) Console.WriteLine("Query results in {0} items. ", count);//

                
                ExportBuilder eb = new ExportBuilder(ws);
                // If count is over 5000 use the ExportBuilder to get the id list
                //string[] 
                ids = count <= 5000 ? queryBuilder.GetKeys(txtQuery.Text) : EwsUtils.GetIdList(eb, cmbExtractType.Text, txtQuery.Text, false);

                var filename = string.Format("{0}", Guid.NewGuid());

                jobId = eb.BuildFromQuery(domain, cmbExtractType.Text, template, txtQuery.Text, filename, Overwrite.True);
                string file = Path.Combine("d:\\Work\\", jobId);

                //jobId = eb.Build(domain, datatype, template, ids, file, ow);

                string result = EwsUtils.RetrieveFile(eb, jobId, file, false);
                MessageBox.Show("Path of the file is " + file + result);
            }
           
        }

        private void chkLogFile_CheckedChanged(object sender, EventArgs e)
        {
            txtLogFile.Text = Application.StartupPath + "\\Log\\";
        }

        private void chkCustomQuery_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCustomQuery.Checked)
            {
                txtQuery.Text = "";
                btnTestQuery.Enabled = true;
            }
            else
            { btnTestQuery.Enabled = false; }
        }

        private void btnSaveDBConfig_Click(object sender, EventArgs e)
        {
            try
            { 
            string appStartUp = Application.StartupPath;
            string cfilename = appStartUp + "\\Config\\config.csv";
                
            if (File.Exists(cfilename))
            {
                //Read Config details
                string[] configlines = IHSUtilities.ReadLog(cfilename);

                if (configlines.Length == 0)
                {
                    //do nothing
                }
                else
                {
                    System.IO.File.WriteAllText(cfilename, string.Empty);
                }
            }
            
            
            bool result = IHSUtilities.WriteLog(txtUserName.Text, cfilename);
            result = IHSUtilities.WriteLog(txtPassword.Text, cfilename);
            result = IHSUtilities.WriteLog(txtDataServer.Text, cfilename);
            result = IHSUtilities.WriteLog(txtDataDB.Text, cfilename);
            result = IHSUtilities.WriteLog(txtDataUsername.Text, cfilename);
            result = IHSUtilities.WriteLog(txtDataPwd.Text, cfilename);
            MessageBox.Show("Config file saved successfully", "Enerdeq Tool", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Could not find file"))
                {
                    MessageBox.Show("Please create the file by saving config", "Enerdeq Tool", MessageBoxButtons.OK);
                }
            }

        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                string LoggerFileName = txtLogFile.Text;
                    //Path.Combine(
                    //  Path.GetDirectoryName(Application.LocalUserAppDataPath),
                    // "log",
                    // "logger.txt");
                System.Diagnostics.Process.Start("notepad.exe", LoggerFileName);
            }
            catch (Exception ex)
            {
                
            }
        }
   }
}
