﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    class EnerdeqServices
    {

        public static string tblName = string.Empty;

        public static void uploadtoDB(DataSet dsRSRData, int iExtractType,string strServer,string strDatabase, string strUname, string strPwd,string[] ids)
        {
            
            SqlConnection con = null;
            try
            {
                //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-2L6A92D\SQLEXPRESS;Initial Catalog=RSTest;Persist Security Info=True;User ID=sa;Password = tiger*()"); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
                con = new SqlConnection(@"Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";Persist Security Info=True;User ID=" + strUname + ";Password = " + strPwd); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
                con.Open();

                //var matches = from dt in dsRSRData.Tables.
                //              let allDaysRecordDay = ((DateTime)allDaysRecord["Date"]).Date
                //              where this.absentsLeavesHolidaysWorks
                //                        .Select(c => ((DateTime)c["Date"]).Date)
                //                        .Contains(allDaysRecordDay)
                //              select allDaysRecord;

                //foreach (var m in matches.ToList())
                //{
                //    m.Delete();
                //}

                
                


                foreach (DataTable dt in dsRSRData.Tables)
                {
                    //foreach (DataColumn dc in dt.Columns)
                    //{
                    //    Console.WriteLine("Table Name : " + dt.TableName + " Column Name : " + dc.ColumnName); 
                    //}
                    tblName = string.Empty;
                    if (iExtractType == 1) //Well
                    {

                        //Well
                        if (dt.TableName.Contains("Header"))
                        {
                            tblName = "Header";
                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_Number_ from Header", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_API_Number_"].ToString());


                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("API Number"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            ////Find IDs
                            //arrray = dsRSRData.Tables[0].Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
                            //retIDs = retIDs.Except(arrray).ToArray();
                            saExisting = null;
                            dsExisting = null;

                        }
                        if (dt.TableName.Contains("Location"))
                        {
                            tblName = "Location";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _UWI_ from Location", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_UWI_"].ToString());

                            
                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("UWI"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;
                        }
                        if (dt.TableName.Contains("TreatmentSummary"))
                        {
                            tblName = "TreatmentSummary";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _UWI_ from TreatmentSummary", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_UWI_"].ToString());

                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("UWI"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;
                        }
                    }

                    if (iExtractType == 2) //Production
                    {

                        //Production
                        if (dt.TableName.Contains("ProductionHeader"))
                        {
                            tblName = "ProductionHeader";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_ from ProductionHeader", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_API_"].ToString());

                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("API"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;
                        }
                        if (dt.TableName.Contains("ProductionWell"))
                        {
                            tblName = "ProductionWell";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_ from ProductionWell", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_API_"].ToString());

                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("API"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;

                        }
                        if (dt.TableName.Contains("ProductionAbstract"))
                        {
                            tblName = "ProductionAbstract";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_ from ProductionAbstract", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_API_"].ToString());

                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("API"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;

                        }
                        if (dt.TableName.Contains("MonthlyProduction"))
                        {
                            tblName = "MonthlyProduction";

                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_+_Month_+convert(nvarchar(4),_Year_) MonYear from MonthlyProduction", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["MonYear"].ToString());

                            //string[] wordlist = { string.Join("", DateTime.Now.ToString("MMM"), DateTime.Now.Year.ToString()) };
                            //DataRow[] rows = dt.AsEnumerable().Where(x => !wordlist.Contains(string.Join("", x.Field<string>("Month"), x.Field<Int32>("Year").ToString()))).ToArray();
                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(string.Join("", x.Field<string>("API"), x.Field<string>("Month"), x.Field<Int32>("Year").ToString()))).ToArray();

                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();


                            saExisting = null;
                            dsExisting = null;


                            //foreach (DataRow drMon in dt.Rows)
                            //{
                            //    if (drMon["_Month_"].Equals(DateTime.Now.Month) && drMon["_Year_"].Equals(DateTime.Now.Year))
                            //    {
                            //        dt.Rows.Remove(drMon);
                            //    }
                            //}


                        }
                        if (dt.TableName.Contains("IPCumNormValues"))
                        {
                            tblName = "IPCumNormValues";


                            DataSet dsExisting = new DataSet();
                            SqlDataAdapter saExisting = new SqlDataAdapter("Select _API_ from IPCumNormValues", con);
                            saExisting.Fill(dsExisting);

                            string[] arrEmpName;
                            DataRow[] brows = dsExisting.Tables[0].Select();
                            arrEmpName = Array.ConvertAll(brows, row => row["_API_"].ToString());


                            DataRow[] rows = dt.AsEnumerable().Where(x => arrEmpName.Contains(x.Field<string>("API"))).ToArray();
                            foreach (DataRow row in rows) dt.Rows.Remove(row);
                            dt.AcceptChanges();

                            saExisting = null;
                            dsExisting = null;

                        }
                    }
                    //if (dt.TableName.Contains("Completion"))
                    //{
                    //    tblName = "Completion";
                    //}

                    if (tblName != string.Empty)
                    {
                        SqlBulkCopy bc = new SqlBulkCopy(con.ConnectionString, SqlBulkCopyOptions.TableLock);
                        bc.DestinationTableName = tblName; // "tblRSRExports";
                        bc.BatchSize = dt.Rows.Count; //dsRSRData.Tables["tblTest"].Rows.Count;               
                        bc.WriteToServer(dt);
                        bc.Close();
                    }

                }
                con.Close();
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                if (con.State==ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public static string[] CheckAndRemoveDupIDs(string[] orgIDs, string strServer, string strDatabase, string strUname, string strPwd)
        {
            try { 
                //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-2L6A92D\SQLEXPRESS;Initial Catalog=RSTest;Persist Security Info=True;User ID=sa;Password = tiger*()"); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
                SqlConnection con = new SqlConnection(@"Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";Persist Security Info=True;User ID=" + strUname + ";Password = " + strPwd); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
                con.Open();

                DataSet dsRSRData = new DataSet();
                //SqlDataAdapter saRSR = new SqlDataAdapter("Select distinct _API_Number_ from Header where Month(_Date_Last_Activity_)<>" + System.DateTime.Now.Month.ToString() + " and Year(_Date_Last_Activity_) <> " + System.DateTime.Now.Year.ToString() , con);
                SqlDataAdapter saRSR = new SqlDataAdapter("Select distinct _API_Number_ from Header where _API_Number_ in (" + String.Join(",", orgIDs)  + ") and  Month(_Date_Last_Activity_) = " + System.DateTime.Now.Month.ToString() + " and Year(_Date_Last_Activity_) = " + System.DateTime.Now.Year.ToString(), con);
                saRSR.Fill(dsRSRData);

                //Find IDs
                string[] arrray = dsRSRData.Tables[0].Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
                string[] retIDs = orgIDs.Except(arrray).ToArray();

                //saRSR = new SqlDataAdapter("Select distinct _API_Number_ from Header where _API_Number_ in (" + String.Join(",", retIDs) + ")", con);
                //saRSR.Fill(dsRSRData);

                ////Find IDs
                //arrray = dsRSRData.Tables[0].Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
                //retIDs = retIDs.Except(arrray).ToArray();


                con.Close();

                
                return retIDs;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
