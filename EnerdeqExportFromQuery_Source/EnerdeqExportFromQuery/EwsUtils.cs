﻿using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using IHSEnergy.Enerdeq.ExportBuilder;
using IHSEnergy.Enerdeq.QueryBuilder;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.IO.Compression;
using System.Data.SqlClient;

namespace IHS.EnergyWebServices.ExportFromQuerySample
{
    public class EwsUtils
    {
        public static int iProgValue;
        public static string GetDatatypeFromQuery (QueryBuilder qb, string query)
        {
            string queryText = query;
            if (qb.GetSavedQueries().Contains(query)) {
                queryText = qb.GetQueryDefinition(query);
            }

            var doc = new XmlDocument();
            doc.LoadXml(queryText);
            var datatypenode = doc.SelectSingleNode("/criterias/criteria/datatype");
            return datatypenode.InnerText;

        }
        public static DataSet RetrieveDataSet(ExportBuilder eb, string jobid, string fileName, bool verbose,int iExtractType, string strServer, string strDatabase, string strUname, string strPwd,bool blnLogEnabled)
        {

            const int maxretry = 300;
            var retry = 0;
            while (!eb.IsComplete(jobid) && retry++ < maxretry)
            {
                Task.Delay(1000).Wait();
                
            }
            iProgValue = iProgValue + 5;
            if (verbose) Console.WriteLine("Pinged for completion {0} times", retry);
            if (retry >= maxretry) throw new Exception("Export didn't build in time.");

            Task.Delay(5000).Wait();
            retry = 0;
            while (!eb.Exists((jobid)) && retry++ < 30)
            {
                Task.Delay(1000).Wait();
                iProgValue = iProgValue + retry;
            }

            if (!eb.Exists(jobid)) throw new Exception("File does not exist.");

            var bytes = eb.Retrieve(jobid, true);
            eb.Delete(jobid);


            string strXmlRecovered = Encoding.UTF8.GetString(bytes);

            MemoryStream memoryStream = new MemoryStream(bytes);

            ZipArchive zipArchive = new ZipArchive(memoryStream);

            DataSet tempDataSet = new DataSet();
            foreach (var zipEntry in zipArchive.Entries)
            {
                if (iExtractType == 1) //Well
                {
                    // Can check file name here and ignore anything in zip we're not expecting
                    if (!(zipEntry.Name.EndsWith("_Header.csv") ||
                        zipEntry.Name.EndsWith("_Location.csv") ||
                        zipEntry.Name.EndsWith("_Treatment Summary.csv")
                        )) continue;
                }
                if (iExtractType == 2) //Production
                {
                    // Can check file name here and ignore anything in zip we're not expecting
                    if (!(zipEntry.Name.EndsWith("_Production Header.csv") ||
                        zipEntry.Name.EndsWith("_Production Well.csv") ||
                        zipEntry.Name.EndsWith("_Production Abstract.csv") ||
                        zipEntry.Name.EndsWith("_Monthly Production.csv") ||
                        zipEntry.Name.EndsWith("_IP-Cum-Norm Values.csv")
                        )) continue;
                }
                // Open zip entry as stream
                Stream extractedFile = zipEntry.Open();

                // Convert stream to memory stream
                MemoryStream extractedMemoryStream = new MemoryStream();
                extractedFile.CopyTo(extractedMemoryStream);

                strXmlRecovered = Encoding.UTF8.GetString(extractedMemoryStream.ToArray());

                string strTableName = string.Empty;
                if (iExtractType == 1) //Well
                {                                    
                    if (zipEntry.Name.EndsWith("_Header.csv"))
                        strTableName = "Header";

                    if (zipEntry.Name.EndsWith("_Location.csv"))
                        strTableName = "Location";

                    if (zipEntry.Name.EndsWith("_Treatment Summary.csv"))
                        strTableName = "TreatmentSummary";
                }

                if (iExtractType == 2) //Production
                {
                    if (zipEntry.Name.EndsWith("_Production Header.csv"))
                        strTableName = "ProductionHeader";
                   
                    if (zipEntry.Name.EndsWith("_Production Well.csv"))
                        strTableName = "ProductionWell";
                    if (zipEntry.Name.EndsWith("_Production Abstract.csv"))
                        strTableName = "ProductionAbstract";

                    if (zipEntry.Name.EndsWith("_Monthly Production.csv"))
                        strTableName = "MonthlyProduction";

                    if (zipEntry.Name.EndsWith("_IP-Cum-Norm Values.csv"))
                        strTableName = "IPCumNormValues";
                }

                DataTable dt = CreateTableFromOutputStream(strXmlRecovered, strTableName, strServer, strDatabase, strUname, strPwd,fileName,blnLogEnabled);

                // Adding DataTable into DataSet  
                if (dt != null)
                    tempDataSet.Tables.Add(dt);

                iProgValue = iProgValue + 10;
            }
            return tempDataSet;
            
        }

       
        private static DataTable CreateTableFromOutputStream(string outputStreamText, string tableName, string strServer, string strDatabase, string strUname, string strPwd,string filename,bool blnLogEnabled)
        {
            SqlConnection con = new SqlConnection(@"Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";Persist Security Info=True;User ID=" + strUname + ";Password = " + strPwd); // @"Server=DESKTOP-2L6A92D\\SQLEXPRESS;Database=RSTest;User Id=sa;Password = tiger*()");
            con.Open();
            //SqlDataAdapter saRSR = new SqlDataAdapter("Select * from dbo.syscolumns where id = (Select id from dbo.sysobjects where type = 'U' and name = '"+ tableName + "')" ,con);
            SqlDataAdapter saRSR = new SqlDataAdapter("Select * from " + tableName.Replace(' ','_'), con);
            DataSet dsRSR = new DataSet();
            saRSR.Fill(dsRSR);
            
            //Process output and return
            string[] split = outputStreamText.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

            if (split.Length >= 2)
            {
                int iteration = 0;
                DataTable table = null;
                foreach (string values in split)
                {
                    if (iteration == 0)
                    {
                        string[] columnNames = SplitString(values);
                        table = new DataTable(tableName);

                        List<DataColumn> columnList = new List<DataColumn>();

                        foreach (string columnName in columnNames)
                        {
                            if (dsRSR.Tables[0].Columns.Contains("_" + columnName.Replace(".", "_").Replace(",", "_").Replace(' ', '_').Replace('/', '_').Replace('(', '_').Replace(')', '_').Replace("-", "_") + "_"))
                            {
                                DataColumn dcName = new DataColumn(columnName, dsRSR.Tables[0].Columns["_" + columnName.Replace(".", "_").Replace(",", "_").Replace(' ', '_').Replace('/', '_').Replace('(', '_').Replace(')', '_').Replace("-", "_") + "_"].DataType);
                                columnList.Add(dcName);
                            }

                        }

                        table.Columns.AddRange(columnList.ToArray());
                    }
                    else
                    {
                        string[] fields = SplitString(values);
                        if (table != null)
                        {
                            for (int iCol=0;iCol<fields.Length;iCol++)
                            {
                                if (table.Columns[iCol].DataType.Name != "String")
                                {
                                    if ((table.Columns[iCol].DataType.Name == "Single" || table.Columns[iCol].DataType.Name.StartsWith("Int")) && fields[iCol] == "")
                                        fields[iCol] = "0";

                                    if (table.Columns[iCol].DataType.Name == "DateTime" && fields[iCol] == "")
                                        fields[iCol] = "1900-01-01";
                                }                                
                            }

                            string result = string.Empty;
                            int iCol2 = 0;
                            foreach (string field in fields)
                            {
                                if (table.Columns[iCol2].DataType.ToString().ToUpper().Contains("STRING") && field.Length > 25) { 
                                    if (blnLogEnabled) IHSUtilities.WriteLog("-------------- " + table.Columns[iCol2].ColumnName + ": "+ field + ": Length is " + field.Length.ToString() + "-------------------" + DateTime.Now.ToString(), filename);
                                }
                                iCol2++;
                            }
                            table.Rows.Add(fields);
                        }
                    }

                    iteration++;
                }


                return table;
            }

            return null;
        }

        private static string[] SplitString(string inputString)
        {
            System.Text.RegularExpressions.RegexOptions options = ((System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace | System.Text.RegularExpressions.RegexOptions.Multiline)
                        | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            Regex reg = new Regex("(?:^|,)(\\\"(?:[^\\\"]+|\\\"\\\")*\\\"|[^,]*)", options);
            MatchCollection coll = reg.Matches(inputString);
            string[] items = new string[coll.Count];
            int i = 0;
            foreach (Match m in coll)
            {
                items[i++] = m.Groups[0].Value.Trim('"').Trim(',').Trim('"').Trim();
            }
            return items;
        }

        public static string RetrieveFile(ExportBuilder eb, string jobid, string fileName, bool verbose)
        {

            const int maxretry = 300;
            var retry = 0;
            while (!eb.IsComplete(jobid) && retry++ < maxretry)
            {
                Task.Delay(1000).Wait();
            }

            if (verbose) Console.WriteLine("Pinged for completion {0} times", retry);
            if (retry >= maxretry) throw new Exception("Export didn't build in time.");

            Task.Delay(5000).Wait();
            retry = 0;
            while (!eb.Exists((jobid)) && retry++ < 30)
            {
                Task.Delay(1000).Wait();
            }

            if (!eb.Exists(jobid)) throw new Exception("File does not exist.");

            var bytes = eb.Retrieve(jobid, true);
            eb.Delete(jobid);
            var path = Path.GetDirectoryName(fileName);
            if (path != null && !Directory.Exists(path)) Directory.CreateDirectory(path);

            File.WriteAllBytes(fileName, bytes);
            return fileName;

        }

        public static string RetrieveReport(IHSEnergy.Enerdeq.ReportBuilder.ReportBuilder rb, string jobid, string fileName, bool verbose = true)
        {

            const int maxretry = 300;
            var retry = 0;
            while (!rb.IsComplete(jobid) && retry++ < maxretry)
            {
                Task.Delay(1000).Wait();
            }

            if (verbose) Console.WriteLine("Pinged for completion {0} times", retry);
            if (retry >= maxretry) throw new Exception("Report didn't build in time.");

            Task.Delay(5000).Wait();
            retry = 0;
            while (!rb.Exists((jobid)) && retry++ < 30)
            {
                Task.Delay(1000).Wait();
            }

            if (!rb.Exists(jobid)) throw new Exception("File does not exist.");

            var bytes = rb.Retrieve(jobid, true);
            rb.Delete(jobid);
            var path = Path.GetDirectoryName(fileName);
            if (path != null && !Directory.Exists(path)) Directory.CreateDirectory(path);

            File.WriteAllBytes(fileName, bytes);
            return fileName;

        }

        public static string[] GetIdList(ExportBuilder eb, string datatype, string query, bool verbose = true)
        {
            string idListTemplate = GetIdListTemplateName(datatype);

            string jobid = eb.BuildFromQuery("US", datatype, idListTemplate, query, Guid.NewGuid().ToString(),
                Overwrite.True);

            var tempfilename = RetrieveFile(eb, jobid, Path.GetTempFileName(), verbose);

            var ids = File.ReadAllLines(tempfilename);
            File.Delete(tempfilename);
            return ids;

        }

        public static string GetIdListTemplateName(string datatype)
        {

            if (String.IsNullOrEmpty(datatype)) throw new ArgumentNullException("datatype");
            if (datatype.Equals("Well", StringComparison.CurrentCultureIgnoreCase)) return "Well Id List";
            if (datatype.Equals("Activity", StringComparison.CurrentCultureIgnoreCase)) return "Well Id List";
            if (datatype.Equals("Activity Data", StringComparison.CurrentCultureIgnoreCase)) return "Well Id List";
            if (datatype.Equals("Production Allocated", StringComparison.CurrentCultureIgnoreCase))
                return "Production Id List";
            if (datatype.Equals("Production Unallocated", StringComparison.CurrentCultureIgnoreCase))
                return "Production Id List";

            throw new ArgumentException(String.Format("'{0}' is an unrecognized datatype value.", datatype), datatype);

        }

        public static string GetUpdatedSinceQuery(string datatype, string updatedSinceDate)
        {
            var query = File.ReadAllText(@"Queries\UpdatedSinceQuery.xml");
            query = query.Replace("$DATATYPE$", datatype);
            query = query.Replace("$UPDATEDSINCEDATE$", updatedSinceDate);
            return query;
        }

        public static string GetDateQuery(string datatype, string fromDate, string attribute = "Last Update")
        {
            var query = "<criterias><criteria type=\"value\"><domain>US</domain><datatype>$DATATYPE$</datatype><attribute_group>Date</attribute_group><attribute>$ATTRIBUTE$</attribute><type>date</type><displaytype /><filter logic=\"greater_than_or_equals\"><value actual=\"$FROMDATE$\" /></filter></criteria></criterias>";

            query = query.Replace("$DATATYPE$", datatype);
            query = query.Replace("$ATTRIBUTE$", attribute);
            query = query.Replace("$FROMDATE$", fromDate);
            return query;
        }

        public static string GetValue(string[] args, int index, string defaultValue)
        {
            return args.Length > index ? args[index] : defaultValue;
        }

        public static List<string> DeleteAllExports(ExportBuilder eb, string startsWith)
        {

            // Get a list of all jobs
            var jobs = eb.List().ToList();

            // Remove all that don't startWith provide string
            if (!string.IsNullOrEmpty(startsWith))
                jobs.RemoveAll((x) => !x.StartsWith(startsWith));

            // Delete remaining jobs
            foreach (var job in jobs)
            {
                eb.Delete(job);
            }

            return jobs;
        }
    }
}
