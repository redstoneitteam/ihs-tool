Example commandline for exporting 'Excel Production Workbook' for all the production unallocated entities defined by the 'ProdUnalloc_OK_Kingfisher' saved query.

The query has been created in Enerdeq Browser.
The export template is one of the standard export templates or oneline export templates.

IHS.EnergyWebServices.ExportFromQuerySample.exe -u YOURUSERNAME -p YOURPASSWORD -s "Excel Production Workbook (Excel 2007, 2010)" -q ProdUnalloc_OK_Kingfisher -c 1000 -v


Additional Links
-------------------
IHS Energy Web Services Portal - https://webservicesportal.ihsenergy.com
	Developers Guide
	CriteriaXML Specification
	
Generate proxy class from a wsdl - http://msdn.microsoft.com/en-us/library/aa529578.aspx
Adding a web service reference - 

Disclaimers
-----------
All code samples are provided “as is” and are provided to demonstrate using the service operation in as simple a way as possible. They include elements of hard coding which should not be used in a production system. The samples do not include exception handling, which should be included in a production system. 
External links are provided throughout this document where relevant in order to provide helpful and useful information from the internet. IHS is not responsible for the content of external Internet sites.
